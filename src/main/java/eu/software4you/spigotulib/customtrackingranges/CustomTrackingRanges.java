package eu.software4you.spigotulib.customtrackingranges;

import eu.software4you.spigot.plugin.ExtendedJavaPlugin;
import eu.software4you.transform.HookInjector;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class CustomTrackingRanges extends ExtendedJavaPlugin {

    private final Map<EntityType, Integer> distances = new ConcurrentHashMap<>();

    @Override
    public void onLoad() {
        HookInjector.hook(new TrackingRangeHook(distances));
    }

    @Override
    public void onEnable() {
        saveDefaultConfig();
        load();
    }

    private void load() {
        saveDefaultLayout();
        var conf = getConf();

        for (String name : conf.getKeys(false)) {
            try {
                EntityType type = EntityType.valueOf(name.toUpperCase());
                int dist = conf.int32(name);
                if (dist > 0) {
                    distances.put(type, dist);
                }
            } catch (IllegalArgumentException | NullPointerException ignored) {
            }
        }
    }

    @Override
    public void onDisable() {
        distances.clear();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("customtrackingrangesreload")) {
            reloadConfig();
            reloadLayout();
            distances.clear();
            load();
            getLayout().sendString(sender, "reloaded");
        }
        return true;
    }
}
