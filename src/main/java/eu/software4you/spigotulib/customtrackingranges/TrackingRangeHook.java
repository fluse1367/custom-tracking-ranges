package eu.software4you.spigotulib.customtrackingranges;

import eu.software4you.reflect.ReflectUtil;
import eu.software4you.transform.Callback;
import eu.software4you.transform.Hook;
import eu.software4you.transform.Hooks;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import java.util.Map;

@Hooks("org.spigotmc.TrackingRange")
public class TrackingRangeHook {
    private final Map<EntityType, Integer> distances;

    TrackingRangeHook(Map<EntityType, Integer> distances) {
        this.distances = distances;
    }

    @Hook("getEntityTrackingRange")
    public void hook_getEntityTrackingRange(Object nmsEntity, int defaultRange, Callback<Integer> cb) {
        EntityType type = ((Entity) ReflectUtil.call(nmsEntity.getClass(), nmsEntity, "getBukkitEntity()")).getType();
        if (distances.containsKey(type)) {
            cb.setReturnValue(distances.get(type));
        }
    }
}
